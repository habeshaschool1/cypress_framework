/// <reference types="Cypress" />

context('Startup', () => {
    beforeEach(() => {
      cy.visit('https://the-internet.herokuapp.com/login');
    });
  
// test valid credentials
it('login with valid credentials and redirect to secure page', () => {
     
      var credentials = {"tomsmith": "SuperSecretPassword!"};

      for (var key in credentials) {
        // Locate and fill the username
        cy.get('#username')
          .type(key)
          .should('have.value', key);
    
        // Locat and fill the password
        cy.get('#password')
          .type(credentials[key])
          .should('have.value', credentials[key]);
  
        // Locate and submit the form
        cy.get('#login').submit();
      
        // Verify that user redirected to the secure
        cy.location('pathname', { timeout: 10000 }).should('eq', '/secure');
      }

    });
    
// test invalid credentials
    it('login with invalid credentials and stay on login page', () => {
     
      var credentials = {"tomsmith": "SuperSecretPassword!!", //incorrect password
                         "tomsmith1": "SuperSecretPassword!", //incorrect username
                         "tomsmith1": "SuperSecretPassword!!", //incorrect username and password
                         "tomsmith ": "SuperSecretPassword!", //username with space after it
                         " tomsmith": "SuperSecretPassword!", //username with space before it 
                         " ": " ", //spaces instead of data
                         " ": "SuperSecretPassword!", //space instead of username
                         "tomsmith": " " //space instead of password
                        };

      for (var key in credentials) {
        // Locate and fill the username
        cy.get('#username')
          .type(key)
          .should('have.value', key);
    
        // Locat and fill the password
        cy.get('#password')
          .type(credentials[key])
          .should('have.value', credentials[key]);
    
        // Locate and submit the form
        cy.get('#login').submit();
        
        // Verify that user stayed on login page
        cy.location('pathname', { timeout: 10000 }).should('eq', '/login');
      }

    });

  });