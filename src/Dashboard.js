import React, { useMemo, useState, useEffect } from 'react';
import axios from 'axios';
import { getUser, removeUserSession } from './Utils/Common';
import Table from "./Table";
 
function Dashboard(props) {
  const user = getUser();
  const [data, setData] = useState([]);

  const columns = useMemo(
    () => [
      {
        // first group - TV Show
        Header: "Agenda Info",
        // First group columns
        columns: [
          {
            Header: "Agenda_id",
            accessor: "result.title"
          },
          {
            Header: "Description",
            accessor: "result.desc"
          },
          {
            Header: "Category",
            accessor: "result.category_id"
          },
          {
            Header: "State",
            accessor: "result.state"
          }
        ]
      },
      {
        // Second group - Details
        Header: "Agenda Reports",
        // Second group columns
        columns: [
          {
            Header: "total_comments",
            accessor: "result.reports.total_comments"
          },
          {
            Header: "total_participants",
            accessor: "result.reports.total_participants"
          },
          {
            Header: "total_dislikes",
            accessor: "result.reports.total_dislikes"
          },
          {
            Header: "total_likes",
            accessor: "result.reports.total_likes"
          }
        ]
      }
    ],
    []
  );

  // handle click event of logout button
  const handleLogout = () => {
    removeUserSession();
    props.history.push('/login');
  }
 
    // Using useEffect to call the API once mounted and set the data
    // useEffect(() => {
    //   (async () => {
    //     const result = await axios("https://api.tvmaze.com/search/shows?q=snow");
    //     setData(result.data);
    //   })();
    // }, []);
    //       <Table columns={columns} data={data} /><br /><br />

    useEffect(() => {
      const fetchData = async () => {
        const result = await axios(
          '/agendas',
        );
   
        setData(result.data);
      };
   
      fetchData();
    }, []);

  return (
    <div>
      Welcome {user.name}!<br /><br />
      <Table columns={columns} data={data} /><br /><br />
      <input type="button" onClick={handleLogout} value="Logout" />
    </div>
  );
}
 
export default Dashboard;