import React, { useState } from 'react';
import axios from 'axios';
import { setUserSession } from './Utils/Common';
import { getToken } from './Utils/Common';
 
function Home(props) {
  const [loading, setLoading] = useState(false);
  const name = useFormInput('');
  const phone = useFormInput('');
  const email = useFormInput('');
  const password = useFormInput('');
  const [error, setError] = useState(null);
  const token = getToken();
  
  // handle button click of signup form
  const handleSignup = () => {
    setError(null);
    setLoading(true);


    axios.post('/authentication/signup', { name: name.value, phone: phone.value, email: email.value, password: password.value }).then(response => {
      setLoading(false);
      //setUserSession(response.data.result.access_token, response.data.result.logged_in_as);
      props.history.push('/login');
    }).catch(error => {
      setLoading(false);
        if (error.response.status === 401) setError(error.response.data.message);
        else setError("Something went wrong. Please try again later.");
    });
  }
 
  return (
    <div>
      Registration<br /><br />
      <div>
        Name
      <br />
        <input type="text" {...name} />
      </div>
      <div>
        Phone(*)
      <br />
        <input type="text" {...phone} />
      </div>
      <div>
        Email(*)
      <br />
        <input type="text" {...email} />
      </div>
      <div style={{ marginTop: 10 }}>
        Password(*)
      <br />
        <input type="password" {...password} autoComplete="new-password" />
      </div>
      {error && <><small style={{ color: 'red' }}>{error}</small><br /></>}<br />
      <input type="button" value={loading ? 'Loading...' : 'Signup'} onClick={handleSignup} disabled={loading} /><br />
    </div>
  );
}

const useFormInput = initialValue => {
  const [value, setValue] = useState(initialValue);
 
  const handleChange = e => {
    setValue(e.target.value);
  }
  return {
    value,
    onChange: handleChange
  }
}

export default Home;
