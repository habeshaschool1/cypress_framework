import React, { useState } from 'react';
import axios from 'axios';
import { setUserSession } from './Utils/Common';
import { getToken } from './Utils/Common';

function Category(props) {
  const [loading, setLoading] = useState(false);
  const title = useFormInput('');
  const desc = useFormInput('');
  const [error, setError] = useState(null);
  const token = getToken();

    // handle button click of category form
    const handleCategory = () => {
      setError(null);
      setLoading(true);


      axios.post('/categories', { title: title.value, desc: desc.value }, { headers: { 'Authorization': 'Bearer ' + token } }).then(response => {
          setLoading(false);
          //setUserSession(response.data.result.access_token, response.data.result.logged_in_as);
        props.history.push('/dashboard');
      }).catch(error => {
        setLoading(false);
        if (error.response.status === 401) setError(error.response.data.message);
        else setError("Something went wrong. Please try again later.");
      });
    }
 
    return (
      <div>
        Category<br /><br />
        <div>
          Title
      <br />
          <input type="text" {...title} />
        </div>
        <div style={{ marginTop: 10 }}>
          Description<br />
          <input type="text" {...desc} />
        </div>
        {error && <><small style={{ color: 'red' }}>{error}</small><br /></>}<br />
        <input type="button" value={loading ? 'Loading...' : 'Add'} onClick={handleCategory} disabled={loading} /><br />
      </div>
    );
  }
 
const useFormInput = initialValue => {
  const [value, setValue] = useState(initialValue);
 
  const handleChange = e => {
    setValue(e.target.value);
  }
  return {
    value,
    onChange: handleChange
  }
}
 
export default Category;